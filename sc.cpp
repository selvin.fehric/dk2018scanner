// <sc.cpp>
#include <iostream> 
#include <string> 
#include "sc.h" 

int main() 
{ 
  std::string line; 
  int linnum=0; 
  while (std::getline(std::cin, line)){ 
    InputBuffer buffer(line); 

    while(buffer.notEmpty()) { 
      Token t; 
      if( 0 != (t = getWs(buffer)).tag ) ; 
      else if( 0 != (t = getKeyword(buffer)).tag ) ; 
      else if( 0 != (t = getId(buffer)).tag ) ; 
      else if( 0 != (t = getNum(buffer)).tag ) ; 
      else if( 0 != (t = getAssign(buffer)).tag ) ; 
      else { 
        int ind = buffer.currentIndex; 
        std::cout << ">> Greska na mjestu ["<<  linnum << ":" << ind 
          << "], na pocetku teksta '" 
          << buffer.getRest() << "'." << std::endl ; 
      } 
      if(t.tag != 0) t.print(); 

    } 
    ++linnum; 
  } 
  std::cout << std::endl; 
  return 0; 
} 
