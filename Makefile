CXXFLAGS = -std=c++11
OBJS = sc.o token.o inputbuffer.o functions.o

# directory for .o files 
OBJDIR := obj
DOBJS := $(addprefix $(OBJDIR)/,$(OBJS))
# ---------------------------

all: scanner 

scanner: $(DOBJS) 
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ $(DOBJS)

$(OBJDIR)/%.o: %.cpp %.h | $(OBJDIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

$(OBJDIR):
	mkdir $(OBJDIR)

.PHONY: clean debug rebuild

debug: CXXFLAGS += -g

debug: all

rebuild: clean all

clean:
	rm -rf $(OBJDIR) 
	rm -f scanner
