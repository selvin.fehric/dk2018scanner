// <inputbuffer.hpp>
#include <iostream> 
#include <string> 
#include "inputbuffer.h"

InputBuffer::InputBuffer(std::string s /* ="" */ ) 
: text(s), bufferLength(s.size()) { }
 

InputBuffer& InputBuffer::operator=(std::string const & t) { 
  text = t; 
  lexemeBegin = currentIndex = 0; 
  bufferLength = t.size(); 
  return *this; 
} 

bool InputBuffer::notEmpty() const{ 
  return(currentIndex < bufferLength) ; 
} 

char InputBuffer::peekNext() const{ 
  if(notEmpty()) 
    return text.at(currentIndex); 
  return 0; 
} 

char InputBuffer::getNext(){ 
  if(notEmpty()) { 
    return text.at(currentIndex++); 
  } 
  return 0; 
} 

std::string InputBuffer::getLexeme() const { 
  return std::string(text, lexemeBegin, currentIndex-lexemeBegin); 
} 

std::string InputBuffer::getRest() { 
  std::string tmp(text, currentIndex); 
  currentIndex = bufferLength; 
  return tmp; 
} 

void InputBuffer::startScan() { lexemeBegin = currentIndex; } 

void InputBuffer::retreat() { currentIndex = lexemeBegin; } 

void InputBuffer::retract(int n /* =1 */) { 
  if(currentIndex >= n)  currentIndex -= n; 
} 

void InputBuffer::backtrack(int n /* =1 */) { 
  if(n == 0) 
    currentIndex = lexemeBegin;
  else
    if(currentIndex >= n)  currentIndex -= n; 
} 

void InputBuffer::print() const{ 
  std::cout << "text: " << std::string(text, currentIndex) << std::endl; 
  std::cout << "currentIndex: " << currentIndex << std::endl; 
  std::cout << "bufferLength: " << bufferLength << std::endl; 
  std::cout << "lexemeBegin: " << lexemeBegin << std::endl; 
  std::cout << "peekNext: " << peekNext() << std::endl; 
} 
