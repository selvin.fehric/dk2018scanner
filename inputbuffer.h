// <inputbuffer.hpp>
#ifndef _INPUTBUFFER_ 
#define _INPUTBUFFER_ 
#include <iostream> 
#include <string> 

class InputBuffer { 
  public: 
    std::string text; 
    int lexemeBegin=0, currentIndex=0; 
    int bufferLength = 0; 
    int prevIndex = 0; 

    InputBuffer(std::string = "");

    InputBuffer& operator=(std::string const & t);
    bool notEmpty() const;
    char peekNext() const;
    char getNext();
    std::string getLexeme() const;
    std::string getRest();
    void startScan() ;
    void retreat() ;

    void retract(int =1);
    void backtrack(int =0);
    void print() const; 
};
#endif
