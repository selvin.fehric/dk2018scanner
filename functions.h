// <functions.h>
#ifndef _FUNCTIONS_ 
#define _FUNCTIONS_ 

#include <string> 
#include "inputbuffer.h" 
#include "token.h"

Token getWs(InputBuffer& b); 
Token getIf(InputBuffer& b); 
Token getKeyword(InputBuffer& b); 
Token getId(InputBuffer& b); 
Token getAssign(InputBuffer& b); 
Token getNum(InputBuffer& b); 

Token wrongToken(InputBuffer & b); 
std::string getTokenName(int); 

#endif
